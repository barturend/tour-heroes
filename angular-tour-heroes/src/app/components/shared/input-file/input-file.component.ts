import { Component, OnInit, Input , EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.css']
})
export class InputFileComponent implements OnInit {
  @Input() displayText:string = "Documento";
  fileName:string;
  file:{ value : any , file : File};
  @Output() onUploadFile :EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.fileName = this.displayText;
  }

  selectFile(e){
    console.log(e);
    this.fileName = e.target.value.split("\\").pop() || this.displayText;
    if(e.target.files && e.target.files[0])
      this.file = e.target.files[0];
    else
      this.file = null;
    this.onUploadFile.emit({ value : e.target.value , file: this.file});
  }
}
